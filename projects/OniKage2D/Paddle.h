#pragma once
#include "kage2dutil/gameobject.h"

class Paddle : public kage::GameObject
{
public:
	Paddle();
	~Paddle();

	void update(float deltaT);
	void onCollision(GameObject *obj);
	void onCollision(b2Fixture *fix);

	sf::Sprite *m_spriteIdle;
	sf::Sprite *m_spriteLeft;		// Additional sprite used for animation purposes
	sf::Sprite *m_spriteRight;		// Additional sprite used for animation purposes
	float m_speed;					// Velocity of paddle on user input
	float m_control;				// Horizontal velocity added to ball by paddle
};
