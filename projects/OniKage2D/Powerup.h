#pragma once
#include "kage2dutil/gameobject.h"

class Powerup : public kage::GameObject
{
public:
	enum class Type
	{
		pu_doubleBalls = 0,
		pu_score2x,
		pu_score4x
	};

	Powerup();
	~Powerup();

	void update(float deltaT);
	void onCollision(GameObject* obj);
	void Powerup::onCollision(b2Fixture* fix);
	void Powerup::changeType(Powerup::Type type);

	sf::Sprite *m_sprite2;			// Additional sprite for collision state
	sf::Sprite *m_sprite3;			// Additional sprite for collision state

	Type m_type;
};
