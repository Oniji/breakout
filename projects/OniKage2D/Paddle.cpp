#include "Paddle.h"
#include "example.h"

Paddle::Paddle()
{
	m_spriteIdle = kage::TextureManager::getSprite("data/paddle.png");
	m_spriteLeft = kage::TextureManager::getSprite("data/paddle-left.png");
	m_spriteRight = kage::TextureManager::getSprite("data/paddle-right.png");
	m_tags.add("Paddle");

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
		.pos(0, 0)
		.userData(this)	// This lets the body know which GameObject owns it
		.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::BoxBuilder()
		.size(3,1)
		.mass(99999)
		.restitution(1)
		.friction(1)
		.build(m_body); // We need to tell the builder which body to attach to

	m_body->SetGravityScale(0);

	m_speed = 15; // Movement speed of the paddle
	m_control = 50; // Ability to adjust direction of ball

	m_sprite = m_spriteIdle;
	kage::centreOrigin(m_sprite);

}

Paddle::~Paddle()
{

}

void Paddle::update(float deltaT)
{
	// Do logic here

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		m_sprite = m_spriteLeft;
		kage::centreOrigin(m_sprite);
		
		if (m_position.x >= 80)
		{ 
			velocity(-20, 0);
		}
		else
		{
			velocity(0, 0);
		}
		
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		m_sprite = m_spriteRight;
		kage::centreOrigin(m_sprite);

		if (m_position.x <= 1840)
		{
			velocity(20, 0);
		}
		else
		{
			velocity(0, 0);
		}
	}
	else
	{

		m_sprite = m_spriteIdle;
		kage::centreOrigin(m_sprite);

		velocity(0, 0);
	}
}

void Paddle::onCollision(GameObject *obj)
{
	//if (obj->m_tags.has("enemy"))
	//{
		//m_dead = true;		// kills itself
		//obj->m_dead = true;	// kills the other object
	//}

	if (obj->m_tags.has("Ball"))
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			obj->addForce(-m_speed * m_control, 0);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			obj->addForce(m_speed * m_control, 0);
		}
	}

	// Player collected a powerup
	if (obj->m_tags.has("Powerup"))
	{
		if (obj->m_tags.has("DoubleBalls"))
		{
			Example::inst().m_puDoubleBall = true;
		}
		else if (obj->m_tags.has("Score2x"))
		{
			Example::inst().m_puScore2x = true;
		}
		else if (obj->m_tags.has("Score4x"))
		{
			Example::inst().m_puScore4x = true;
		}


	}
}

void Paddle::onCollision(b2Fixture* fix)
{
	if ((int)(fix->GetUserData()) == 2)
	{
		velocity(0, 0);
	}
}
