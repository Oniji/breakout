#pragma once

#include "app.h"
#include "kage2dutil/physics.h"
#include "ball.h"
#include "brick.h"
#include "paddle.h"
#include "powerup.h"

class Example : public App
{
public:
	Example();
	virtual ~Example();
	virtual bool start();
	virtual void update(float deltaT);
	virtual void render();
	virtual void cleanup();
	static Example &inst();
	void Example::createBrickGrid();
	void Example::destroyBrickGrid();
	void Example::destroyExtraBalls();
	void Example::destroyPowerups();

	bool isDebugMode = false;			// Shows/Hides Debug Colliders and Helpers
	sf::Sprite *m_backgroundSprite;		// Background image of Window
	sf::Sprite *m_spriteGameOver;		// Game over image
	int m_score;						// Tracks player's score
	int m_highscore;					// Tracks high score for current game session
	int m_lives;						// Tracks player's remaining lives
	int m_hitBaseScore;					// Base score on brick hit
	float m_scoreMulti;				// Current score multiplier
	bool m_gameState;					// Is an active game being played?
	bool m_winState;					// Has the player won?
	bool m_spawnPowerup;				// Force spawn a powerup
	bool m_puScore2x;					// Powerup state: Score Multiplier x 2
	bool m_puScore4x;					// Powerup state: Score Multiplier x 4
	bool m_puDoubleBall;				// Powerup state: Double Ball
	float m_puMultiTimer;				// Powerup score multiply timer
	Paddle* paddle;						// Paddle Game Object
	Ball* ball;							// Primary Ball Game Object

};
