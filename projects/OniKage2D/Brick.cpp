#include "Brick.h"
#include "example.h"

Brick::Brick()
{
	m_sprite = kage::TextureManager::getSprite("data/brick1.png");
	m_sprite2 = kage::TextureManager::getSprite("data/brick2.png");		// Additional collision states of brick
	m_sprite3 = kage::TextureManager::getSprite("data/brick3.png");		// Additional collision states of brick
	kage::centreOrigin(m_sprite);
	m_tags.add("Brick");

	m_spriteID = 1;						// Used to check if sprite matches collision state
	m_allowedCollisions = 1;			// Number of remaining allowed collisions

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
		.pos(0, 0)
		.userData(this)					// This lets the body know which GameObject owns it
		.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::BoxBuilder()
		.size(1.5, 0.6)
		.mass(9999)						// Behave like a kinematic object
		.restitution(1)					// Bounciness of the surface
		.build(m_body);					// We need to tell the builder which body to attach to

	m_body->SetGravityScale(0);			// Unaffected by gravity

}

Brick::~Brick()
{

}

void Brick::update(float deltaT)
{

	// Updates sprite if it does not match the collision state
	if (m_allowedCollisions != m_spriteID && m_sprite)
	{
		if (m_allowedCollisions == 3)
		{
			m_sprite = m_sprite3;
		}
		else if (m_allowedCollisions == 2)
		{
			m_sprite = m_sprite2;
		}
		kage::centreOrigin(m_sprite);
	}
}

void Brick::onCollision(GameObject* obj)
{

	// When brick collides with ball
	if (obj->m_tags.has("Ball"))
	{
		m_allowedCollisions--;	// Remove number of brick collisions remaining

		if (m_allowedCollisions <= 0)
		{
			Example::inst().m_score = Example::inst().m_score + (Example::inst().m_hitBaseScore * Example::inst().m_scoreMulti * 3);				// Increment game score for hitting block
			int r = rand() % 5;									// Chance of spawning powerup
			if (r == 1) Example::inst().m_spawnPowerup = true;		// Force spawn powerup
			m_dead = true;										// Destroy brick
		}
		else 
		{
			// Update sprite after updating collision state
			if (m_sprite)
			{
				if (m_allowedCollisions == 2)
				{
					m_sprite = kage::TextureManager::getSprite("data/brick2.png");
					Example::inst().m_score = Example::inst().m_score + Example::inst().m_hitBaseScore * Example::inst().m_scoreMulti;				// Increment game score for hitting block
				}
				else if (m_allowedCollisions == 1)
				{
					m_sprite = kage::TextureManager::getSprite("data/brick1.png");
					Example::inst().m_score = Example::inst().m_score + (Example::inst().m_hitBaseScore * Example::inst().m_scoreMulti * 3);		// Increment game score for hitting block
				}
				kage::centreOrigin(m_sprite);
			}

		}
	}
}
