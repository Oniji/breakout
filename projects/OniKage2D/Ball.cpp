#include "Ball.h"
#include "example.h"

Ball::Ball()
{
	m_sprite = kage::TextureManager::getSprite("data/extraball.png");
	kage::centreOrigin(m_sprite);
	m_tags.add("Ball");

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
		.pos(0, 0)
		.userData(this)	// This lets the body know which GameObject owns it
		.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::CircleBuilder()
		.radius(0.3)
		.mass(0)				
		.build(m_body);			// We need to tell the builder which body to attach to

	m_body->SetGravityScale(0);	// Unaffected by gravity
	m_maxSpeedFactor = 12;		// Maximum velocity of the ball
	m_maxSpinSpeed = 50;		// Maximum Angular Velocity
	m_minVerticalSpeed = 4;		// Minimum vertical speed
	m_isBallDead = false;		// Ball state without destroying object
	m_isPrimaryBall = false;		// Is this the primary game ball

}

Ball::~Ball()
{

}

void Ball::update(float deltaT)
{
	// Enforce speed limit of ball
	if (m_isBallDead != true)
	{
		kf::Vector2 vel = velocity();
		vel.normalise();
		vel = vel * m_maxSpeedFactor;

		// Prevents ball from getting horizontally locked
		if (vel.y >= 0 && vel.y < m_minVerticalSpeed)
		{
			vel.y = m_minVerticalSpeed;
		}
		else if (vel.y > -m_minVerticalSpeed && vel.y < 0)
		{
			vel.y = -m_minVerticalSpeed;
		}

		velocity(vel); // Update ball velocity

		// If ball exceeding maximum angular velocity
		if (abs(this->m_body->GetAngularVelocity()) > m_maxSpinSpeed)
		{
			this->m_body->SetAngularVelocity((this->m_body->GetAngularVelocity() / this->m_body->GetAngularVelocity()) * m_maxSpinSpeed); // Sets to maximum
		}

	}

}

void Ball::onCollision(GameObject* obj)
{

	// If colliding with paddle, set angular momentum
	if (obj->m_tags.has("Paddle"))
	{
		float angularVel = this->m_body->GetAngularVelocity();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			if (angularVel >= 0)
			{
				this->m_body->SetAngularVelocity(angularVel + 10);
			}
			else
			{
				this->m_body->SetAngularVelocity(angularVel - 10);
			}

		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			if (angularVel >= 0)
			{
				this->m_body->SetAngularVelocity(angularVel - 10);
			}
			else
			{
				this->m_body->SetAngularVelocity(angularVel + 10);
			}
		}
	}

	if (obj->m_tags.has("Brick"))
	{
		// Random chance to spawn a power up. 30% chance?
	}

}

void Ball::onCollision(b2Fixture* fix)
{

	// If ball collides with ground collider
	if ((int)(fix->GetUserData()) == 1)
	{
		if (m_isPrimaryBall)
		{
			m_isBallDead = true;						// Set to fake dead state
			velocity(0, 0);								// Stops primary ball
			this->position(-100, -100);					// Move ball outside of window
			Example::inst().m_lives--;					// Player loses a life
			Example::inst().destroyExtraBalls();		// Destroy all extra balls in Window
			Example::inst().destroyPowerups();			// Cleanup powersup from Window
			Example::inst().m_puMultiTimer = 0.0f;		// Reset buff timer
			Example::inst().m_scoreMulti = 1.0f;		// Reset score multiplier
		}
		else {
			m_dead = true;
		}

	}
}

// Makes this ball the primary ball that interacts with game state
void Ball::makePrimary()
{
	m_isPrimaryBall = true;
	m_sprite = kage::TextureManager::getSprite("data/ball.png");
	kage::centreOrigin(m_sprite);
	m_tags.add("Primary");
}


