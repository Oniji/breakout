#include "Powerup.h"
#include "example.h"

Powerup::Powerup()
{
	m_sprite = kage::TextureManager::getSprite("data/pu_doubleballs.png");
	m_sprite2 = kage::TextureManager::getSprite("data/pu_score2x.png");		// Powerup Sprite Variant #2
	m_sprite3 = kage::TextureManager::getSprite("data/pu_score4x.png");		// Powerup Sprite Variant #3
	kage::centreOrigin(m_sprite);
	m_tags.add("Powerup");

	m_physicsStyle = GameObject::e_psBox2D;

	// Make a Box2D body
	m_body = kage::Physics::BodyBuilder()
		.pos(0, 0)
		.userData(this)					// This lets the body know which GameObject owns it
		.build();

	// Make a fixture (collision shape) for the body
	kage::Physics::BoxBuilder()			
		.sensor(true)					// Make object a sensor
		.size(0.6, 0.6)
		.mass(0)						// Behave like a kinematic object
		.restitution(0)					// Bounciness of the surface
		.build(m_body);					// We need to tell the builder which body to attach to

	m_body->SetGravityScale(0);			// Unaffected by gravity

	m_type = Type::pu_doubleBalls;		// Default powerup type		
	m_tags.add("DoubleBalls");			// Default powerup type

}

Powerup::~Powerup()
{

}

void Powerup::update(float deltaT)
{

}

void Powerup::onCollision(GameObject* obj)
{

	// Player has collected powerup with paddle
	if (obj->m_tags.has("Paddle"))
	{
		// Player collected powerup by type
		if (m_type == Type::pu_score2x) Example::inst().m_puScore2x = true;
		else if (m_type == Type::pu_score4x) Example::inst().m_puScore4x = true;
		else Example::inst().m_puDoubleBall = true;

		m_dead = true;			// Destroy powerup
	}

}

void Powerup::onCollision(b2Fixture* fix)
{
	// If ball collides with ground collider
	if ((int)(fix->GetUserData()) == 1)
	{
		m_dead = true;
	}
}

// Change the type of powerup
void Powerup::changeType(Powerup::Type type)
{
	if (type == Type::pu_score2x)
	{
		m_sprite = kage::TextureManager::getSprite("data/pu_score2x.png");
		m_tags.add("Score2x");
		m_tags.remove("DoubleBalls");
		m_tags.remove("Score4x");
	}
	else if (type == Type::pu_score4x)
	{
		m_sprite = kage::TextureManager::getSprite("data/pu_score4x.png");
		m_tags.add("Score4x");
		m_tags.remove("DoubleBalls");
		m_tags.remove("Score2x");
	}
	else
	{
		m_sprite = kage::TextureManager::getSprite("data/pu_doubleballs.png");
		m_tags.add("DoubleBalls");
		m_tags.remove("Score4x");
		m_tags.remove("Score2x");
	}

}
