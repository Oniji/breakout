#include "app.h"
#include "example.h"

#include "SFML/Graphics.hpp"
#include "kage2dutil/system.h"
#include "kage2dutil/imgui.h"
#include "kage2dutil/imgui-SFML.h"
#include "kage2dutil/physics.h"

App::App()
{
}

App::~App()
{
}

bool App::init()
{
	// This sets the working directory to where the executable is.
	kage::initDirectory();

	// Start the KF log system, tell it to log to the console and a file.
	kf::LogSystem::getDefault().addCout();
	kf::LogSystem::getDefault().addFile("base.log");
	kf_log("Started");

	m_window.create(sf::VideoMode(1920, 1080, 32), "Kage2D");
	m_window.setFramerateLimit(60);
	
	if (!m_font.loadFromFile("data/bluehigh.ttf"))
	{
		return false;
	}
	ImGui::CreateContext();
	ImGui::SFML::Init(m_window);
	
	// Initialise the physics system. Set the default gravity to 9.8m/s^2 down.
	kage::Physics::init(b2Vec2(0, 9.8));

	return true;
}

bool App::start()
{
	return true;
}

void App::update(float deltaT)
{

}

void App::render()
{

}

void App::run()
{
	m_running = true;

	if (!init())
	{
		return;
	}

	if (!start())
	{
		return;
	}

	while (m_window.isOpen() && m_running)
	{

		//bool paused;
		//bool lastKeyState;

		// Events are things such as keys being pressed, the window closing, etc.
		// There could be several events waiting for us, so use a loop to process them all.
		sf::Event event;
		while (m_window.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			if (event.type == sf::Event::Closed)
			{
				m_window.close();
				break;
			}
		}
		sf::Time deltaT_sfml = m_clock.restart();
		float deltaT = deltaT_sfml.asSeconds();
		ImGui::SFML::Update(m_window, deltaT_sfml);

		/*
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Pause))
		{
			// Do something with pause
		}
		if (paused)
		{

		}

		*/

		kage::Physics::update(1.0 / 60.0);
		kage::World::update(1.0 / 60.0);
		update(1.0 / 60.0);

		//kage::World::inspector();

		// Clear the window.
		m_window.clear();

		render();
		kage::World::render(m_window);
		ImGui::SFML::Render(m_window);
	

		// Score and Lives UI

		// Score Text
		sf::Text scoreText;
		scoreText.setFont(m_font);
		scoreText.setString("Score " + std::to_string(Example::inst().m_score));
		scoreText.setFillColor(sf::Color::White);
		scoreText.setStyle(sf::Text::Bold);

		// Lives Text
		sf::Text livesText;
		livesText.setFont(m_font);
		livesText.setStyle(sf::Text::Bold);
		livesText.setString("Lives " + std::to_string(Example::inst().m_lives));
		livesText.setPosition(50.0f, 88.0f);
		livesText.setCharacterSize(50);

		// High score Text
		sf::Text highScoreText;
		highScoreText.setFont(m_font);
		highScoreText.setStyle(sf::Text::Bold);
		highScoreText.setString(std::to_string(Example::inst().m_highscore));
		highScoreText.setPosition(980.0f, 580.0f);
		highScoreText.setCharacterSize(62);

		// Multiplier Text
		sf::Text scoreMultiplierText;
		scoreMultiplierText.setFont(m_font);
		scoreMultiplierText.setStyle(sf::Text::Bold);
		scoreMultiplierText.setCharacterSize(50);

		// Multipler Timer Text
		sf::Text scoreTimerText;
		scoreTimerText.setFont(m_font);
		scoreTimerText.setStyle(sf::Text::Bold);
		scoreTimerText.setCharacterSize(32);

		// Set text colour based on remaining lives
		if (Example::inst().m_lives <= 0) livesText.setFillColor(sf::Color::Red);
		else if (Example::inst().m_lives == 1 || Example::inst().m_lives == 2) livesText.setFillColor(sf::Color::Yellow);
		else livesText.setFillColor(sf::Color::White);

		if (Example::inst().m_gameState == true)
		{
			scoreText.setPosition(50.0f, 40.0f);		// Move score to top left of window
			scoreText.setCharacterSize(50);			// Resize score font
			m_window.draw(livesText);				// Only draw lives if game is active

			// Show continue text
			if (Example::inst().ball->m_isBallDead == true)
			{
				sf::Sprite *spriteContinue = kage::TextureManager::getSprite("data/continue.png");
				spriteContinue->setPosition(750.0f, 640.0f);
				kage::centreOrigin(spriteContinue);
				m_window.draw(*spriteContinue);
			}
			else
			{
				if (Example::inst().m_scoreMulti > 1.0f)
				{
					scoreMultiplierText.setString(std::to_string((int)Example::inst().m_scoreMulti) + "X");
					scoreMultiplierText.setPosition(50.0f, 134.0f);

					// Set colour of text based on multiplier
					if (Example::inst().m_scoreMulti < 4) scoreMultiplierText.setFillColor(sf::Color::Yellow);
					else if (Example::inst().m_scoreMulti == 4) scoreMultiplierText.setFillColor(sf::Color::Red);
					else scoreMultiplierText.setFillColor(sf::Color::Magenta);

					scoreTimerText.setString(std::to_string((int)Example::inst().m_puMultiTimer) + " secs");
					scoreTimerText.setPosition(120.0f, 148.0f);

					m_window.draw(scoreMultiplierText);
					m_window.draw(scoreTimerText);
				}
				else
				{
					scoreMultiplierText.setPosition(-500.0f, -500.0f);
					scoreTimerText.setPosition(-500.0f, -500.0f);

					m_window.draw(scoreMultiplierText);
					m_window.draw(scoreTimerText);
				}

			}
		}
		else if (Example::inst().m_gameState == false)
		{
			if (Example::inst().m_winState == true)
			{
				sf::Sprite *spriteYouWin = kage::TextureManager::getSprite("data/youwin.png");
				spriteYouWin->setPosition(960.0f, 540.0f);
				kage::centreOrigin(spriteYouWin);
				m_window.draw(*spriteYouWin);			// Only draw game over sprite when game state is false
			}
			else if (Example::inst().m_winState == false)
			{
				// Display Game Over screen
				sf::Sprite *spriteGameOver = kage::TextureManager::getSprite("data/gameover.png");
				spriteGameOver->setPosition(960.0f, 540.0f);
				kage::centreOrigin(spriteGameOver);
				m_window.draw(*spriteGameOver);			// Only draw game over sprite when game state is false
			}

			// Move score text to the game over screen
			scoreText.setString(std::to_string(Example::inst().m_score));
			scoreText.setPosition(980.0f, 522.0f);
			scoreText.setCharacterSize(62);

			m_window.draw(highScoreText);
		}

		m_window.draw(scoreText);

		// Calling display will make the contents of the window appear on screen (before this, it was kept hidden in the back buffer).
		m_window.display();
	}
	
	cleanup();
}

void App::cleanup()
{

}

sf::RenderWindow &App::getWindow()
{
	return m_window;
}

sf::Clock &App::getClock()
{
	return m_clock;
}

