#include "example.h"

Example::Example(): App()
{
}

Example::~Example()
{
}

Example &Example::inst()
{
	static Example s_instance;
	return s_instance;
}

bool Example::start()
{
	
	// Game State variables
	m_score = 0;				// Current player score
	m_highscore = 0;			// Current session high score
	m_lives = 3;				// Player Lives
	m_hitBaseScore = 10;		// Base score for hitting bricks
	m_scoreMulti = 1.0f;			// Current score multiplier
	m_gameState = true;			// Will use enum for game state next time
	m_winState = false;
	m_spawnPowerup = false;		// Spawn a random powerup
	m_puScore2x = false;		// Spawn power-up (Score Multiplier 2x)
	m_puScore4x = false;		// Spawn power-up (Score Multiplier 4x)
	m_puDoubleBall = false;		// Spawn power-up (Double Ball)
	m_puMultiTimer = 0.0f;		// Score Multiplier Buff Timer (in seconds)


	// Floor, left wall and right wall static colliders.
	kage::Physics::BoxBuilder().pos(kf::Vector2(15, 18)).size(32, 1).restitution(1).userData(1).build(kage::Physics::getDefaultStatic());	// Bottom Collider
	kage::Physics::BoxBuilder().pos(kf::Vector2(0, 8)).size(1, 18).restitution(1).userData(2).build(kage::Physics::getDefaultStatic());		// Left Collider
	kage::Physics::BoxBuilder().pos(kf::Vector2(30, 8)).size(1, 18).restitution(1).userData(2).build(kage::Physics::getDefaultStatic());	// Right Collider
	kage::Physics::BoxBuilder().pos(kf::Vector2(15, 0)).size(30, 1).restitution(1).userData(2).build(kage::Physics::getDefaultStatic());	// Top Collider

	// Setup Window Background
	m_backgroundSprite = kage::TextureManager::getSprite("data/background-snow.png");
	sf::Vector2u resolution = m_backgroundSprite->getTexture()->getSize();
	m_backgroundSprite->setScale(float(m_window.getSize().x) / resolution.x, float(m_window.getSize().y) / resolution.y);

	//Initialise Objects
	paddle = kage::World::build<Paddle>();
	ball = kage::World::build<Ball>();
	ball->makePrimary();

	//Setup object positions (in metres and not pixels)
	ball->position(11, 10);
	paddle->position(15, 15);

	createBrickGrid(); // Creates a brick grid

	// Initial movement of the ball
	ball->addForce(500, 500);
	ball->m_body->SetAngularVelocity(10);

	return true;
}

void Example::update(float deltaT)
{
	// Exit game on user input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && m_window.hasFocus())
	{
		m_running = false;
	}

	// Update buff timer
	if (m_puMultiTimer > 0.0f)
	{
		m_puMultiTimer = m_puMultiTimer - deltaT;
	}
	else
	{
		m_puMultiTimer = 0.0f;
		m_scoreMulti = 1.0f;
	}


	if (m_spawnPowerup == true)
	{
		m_spawnPowerup = false;
		Powerup *powerup = kage::World::build<Powerup>();		// Create new powerup
		int r = rand() % 3;
		// Generate a powerup of random type
		if (r == 0) powerup->changeType(Powerup::Type::pu_doubleBalls);
		else if (r == 1) powerup->changeType(Powerup::Type::pu_score2x);
		else if (r == 2) powerup->changeType(Powerup::Type::pu_score4x);
		else powerup->changeType(Powerup::Type::pu_doubleBalls);

		powerup->position(rand() % 30, 1);						// Spawn in a random location at top of window
		powerup->addForce(0, 300);								// Powerup falls at a constant rate
	}


	// Game Over State
	if (m_lives == 0)
	{
		m_gameState = false;
		destroyExtraBalls();
		destroyPowerups();
		m_puMultiTimer = 0.0f;		// Reset buff timer
		m_scoreMulti = 1.0f;		// Reset score multiplier
		if (m_score > m_highscore)
		{
			m_highscore = m_score;
		}
	}

	if (kage::World::findByTag("Brick") == false && m_winState == false)
	{
		// Win Condition
		m_gameState = false;
		m_winState = true;
		destroyExtraBalls();		// Cleanup extra balls in Window
		destroyPowerups();			// Cleanup powersup from Window
		ball->velocity(0, 0);
		m_score = m_score + 100;	// Add victory points for clearing 
	}

	// Space to initiate a ball reset
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		// Is Ball in a dead state
		if (ball->m_isBallDead == true)
		{
			ball->m_isBallDead = false;
			ball->position(10, 9);
			ball->addForce(500, 500);
			ball->m_body->SetAngularVelocity(10);

			// Restart game and reset to initial game state
			if (m_lives <= 0)
			{
				m_gameState = true;
				m_winState = false;
				m_lives = 3;
				m_score = 0;
				createBrickGrid();
			}
		}
		else if (ball->m_isBallDead != true && m_winState == true)
		{
			// Reset game state after player win
			m_gameState = true;
			m_winState = false;
			m_lives = 3;
			m_score = 0;
			createBrickGrid();
		}

	}

	// Powerups Enabled
	if (m_puDoubleBall)
	{
		m_puDoubleBall = false;
		Ball *extraBall = kage::World::build<Ball>();
		extraBall->position(12, 1);
		extraBall->addForce(500, 500);
		extraBall->m_body->SetAngularVelocity(10);
	}
	if (m_puScore4x || m_puScore2x)
	{
		if (m_scoreMulti < 8)
		{
			if (m_puScore4x) 
			{
				m_scoreMulti = m_scoreMulti * 4;
				m_puScore4x = false;
			}
			if (m_puScore2x)
			{
				m_scoreMulti = m_scoreMulti * 2;
				m_puScore2x = false;
			}
		}
		else
		{
			m_puScore4x = false;
			m_puScore2x = false;
		}
		m_puMultiTimer = 10.0f;		// Set buff timer to 10 seconds
	}

}

void Example::render()
{

	m_window.draw(*m_backgroundSprite);
	// The next line draws the physics debug info. This should be removed in a final release.
	if (Example::inst().isDebugMode == true) kage::Physics::debugDraw(&m_window, 64);
}

void Example::cleanup()
{
	delete m_backgroundSprite;
}

// Randomly picks from various premade grid patterns
void Example::createBrickGrid()
{

	destroyBrickGrid();				// Clear existing brick grid

	int randomGrid = rand() % 3;	// Choose a random pre-made brick configuration

	if (randomGrid == 0)
	{
		// Create Brick Grid #1
		for (int i = 0; i < 12; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				if (j >= (6 - i) && j > (i - 6))
				{
					Brick* brick = kage::World::build<Brick>();					// Initialise a new brick object
					brick->position(4 + (2 * i), 3 + (0.8 * j));				// Set position of new brick

					// Brick pattern based on number of collisions
					if (j >= 0 && j < 3) brick->m_allowedCollisions = 1;
					else if (j >= 3 && j < 5) brick->m_allowedCollisions = 2;
					else brick->m_allowedCollisions = 3;
				}

			}
		}

	}
	else if (randomGrid == 1)
	{
		// Create Brick Grid #2
		for (int i = 0; i < 12; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				if (j <= i && j < (12 - i))
				{
					Brick* brick = kage::World::build<Brick>();					// Initialise a new brick object
					brick->position(4 + (2 * i), 3 + (0.8 * j));				// Set position of new brick

					// Brick pattern based on number of collisions
					brick->m_allowedCollisions = 3;
				}


			}
		}
	}
	else
	{
		// Create Brick Grid #3
		for (int i = 0; i < 12; i++)
		{
			for (int j = 0; j < 6; j++)
			{

				Brick* brick = kage::World::build<Brick>();					// Initialise a new brick object
				brick->position(4 + (2 * i), 3 + (0.8 * j));				// Set position of new brick

				// Brick pattern based on number of collisions
				if (j >= 0 && j < 3) brick->m_allowedCollisions = 3;
				else if (j >= 3 && j < 5) brick->m_allowedCollisions = 2;	
				else brick->m_allowedCollisions = 1;

			}
		}

	}

}

void Example::destroyBrickGrid()
{
	// Find all Brick GameObjects in the World
	std::vector<kage::GameObject *> bricks = kage::World::findAllByTag("Brick");

	// Iterate through vector of all Bricks and kill them
	for (std::vector<kage::GameObject *>::iterator it = bricks.begin(); it != bricks.end(); ++it)
	{
		kage::GameObject * currentBrick;
		currentBrick = *it;
		currentBrick->m_dead = true;
	}

}

void Example::destroyExtraBalls()
{
	// Find all Ball GameObjects in the World
	std::vector<kage::GameObject *> balls = kage::World::findAllByTag("Ball");

	// Iterate through vector of all Balls and kill them
	for (std::vector<kage::GameObject *>::iterator it = balls.begin(); it != balls.end(); ++it)
	{
		kage::GameObject * currentBall;
		currentBall = *it;

		// Do not destroy primary game ball
		if (!currentBall->m_tags.has("Primary"))
		{
			currentBall->m_dead = true;
		}
	}

}

// Destroys all powersup in the Window
void Example::destroyPowerups()
{
	// Find all Powerups GameObjects in the World
	std::vector<kage::GameObject *> powerups = kage::World::findAllByTag("Powerup");

	// Iterate through vector of all Powerups and kill them
	for (std::vector<kage::GameObject *>::iterator it = powerups.begin(); it != powerups.end(); ++it)
	{
		kage::GameObject * currentPowerup;
		currentPowerup = *it;
		currentPowerup->m_dead = true;

	}

}
