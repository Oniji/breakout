#pragma once
#include "kage2dutil/gameobject.h"
#include "Paddle.h"

class Ball : public kage::GameObject
{
public:
	Ball();
	~Ball();

	void update(float deltaT);
	void onCollision(GameObject *obj);
	void onCollision(b2Fixture *fix);
	void Ball::makePrimary();	// Makes this ball the primary ball

	float m_maxSpeedFactor;		// Maximum velocity of ball
	float m_maxSpinSpeed;		// Maximum angular velocity of ball
	float m_minVerticalSpeed;	// Minimum vertical velocity to prevent stalls
	bool m_isBallDead;			// Death state of the ball without destroying object
	bool m_isPrimaryBall;		// Is this the primary game ball

};
