#pragma once
#include "kage2dutil/gameobject.h"

class Brick : public kage::GameObject
{
public:
	Brick();
	~Brick();

	void update(float deltaT);
	void onCollision(GameObject* obj);

	int m_allowedCollisions;		// Remaining number of allowed collisions with balls
	int m_spriteID;					// Used to check sprite updates to collision state
	sf::Sprite *m_sprite2;			// Additional sprite for collision state
	sf::Sprite *m_sprite3;			// Additional sprite for collision state
};
